import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

// Create Express APP
const app:Express=express();
const port: string | number =process.env.PORT || 8000;


// Define the first Route of APP
app.get("/", (req: Request, res: Response) => {
    // Send Hello Word
    res.send("Hola mundo");
  });

// Define the first Route of APP
app.get("/hello", (req: Request, res: Response) => {
    // Send Hello Word
    res.send("hola a GET Route: Hola mundo");
  });

// Execute APP and Listen Request to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: running at http://localhost:${port}`);
  });